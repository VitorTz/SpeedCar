from pygame import display, Surface
from .image import load_image
from .settings import SURFACE_INFO
from pathlib import Path


def load_surface() -> Surface:
    surface = display.set_mode((SURFACE_INFO["width"], SURFACE_INFO["height"]))
    display.set_caption(SURFACE_INFO["name"])
    icon = load_image(Path(SURFACE_INFO["icon"]))
    if icon:
        display.set_icon(icon)
    return surface