import pygame
from pathlib import Path


def get_font(ttf_font: Path, size: int = 40):
    if ttf_font.exists() and ttf_font.suffix.lower() == ".ttf":
        return pygame.font.Font(ttf_font, size)


def get_image_text(msg, color: tuple, font: pygame.font.Font = None, path_font: Path = None, size: int = 40):
    if font:
        return font.render(msg, True, color)
    return get_image_text(msg, color, get_font(path_font, size))

