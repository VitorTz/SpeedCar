from .default_activity import Activity, GameEvents, GameStats
import pygame


class Gaming(Activity):

    def __init__(self, surface: pygame.Surface, game_events: GameEvents, game_stats: GameStats):
        super().__init__(surface, game_events, game_stats, "Gaming")
        self.bg_color = (0, 0, 230)

    def draw_activity(self):
        self.fill_surface()
        self.update_screen()
