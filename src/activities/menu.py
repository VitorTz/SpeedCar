from .default_activity import Activity, GameEvents, GameStats
from ..settings import FONT
from ..button import Button
import pygame


class Menu(Activity):

    __str_jogar = "PLAY"
    __str_placar = "SCORE"
    __str_select_car = "SELECT CAR"
    __selected_button_size = 60
    __selected_button_color = (0, 0, 0)

    def __init__(self, surface: pygame.Surface, game_events: GameEvents, game_stats: GameStats):
        super().__init__(surface=surface, game_events=game_events, game_stats=game_stats, label="Menu")
        self.bg_color = (230, 0, 0)
        self.__btn_play = Button(FONT["wonder"], self.surface_rect.centerx, self.surface_rect.top + 200, self.str_jogar)
        self.__btn_placar = Button(FONT["wonder"], self.surface_rect.centerx, self.__btn_play.rect.bottom + 100, self.str_placar)
        self.__btn_select_car = Button(FONT["wonder"], self.surface_rect.centerx, self.__btn_placar.rect.bottom + 100, self.str_select_car)
        self.__selected_button = self.__btn_play

    def __set_selected_button(self, new_selected_button: Button):
        self.__selected_button.reset()
        new_selected_button.size = self.__selected_button_size
        new_selected_button.color = self.__selected_button_color

    def __switch_button(self):
        if self.game_events.get_status_key(pygame.K_UP):

    def draw_activity(self):
        self.fill_surface()
        for btn in (self.__btn_placar, self.__btn_play, self.__btn_select_car):
            btn.draw_buttom(self.surface)
        self.update_screen()
