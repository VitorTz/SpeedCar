import pygame
from .default_activity import Activity, GameEvents, GameStats


class MainActivity(Activity):

    def __init__(self, surface: pygame.Surface, game_events: GameEvents, game_stats: GameStats):
        super().__init__(surface=surface, game_events=game_events, game_stats=game_stats, label="Main")

    def draw_activity(self):
        self.fill_surface()
        self.update_screen()

