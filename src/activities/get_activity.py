from enum import Enum, auto
from pygame import Surface
from .main import MainActivity
from .menu import Menu
from ..game_events import GameEvents
from ..game_stats import GameStats


class ActivityType(Enum):

    MAIN = auto()
    MENU = auto()


ACTIVITIES = {
    ActivityType.MENU: Menu,
    ActivityType.MAIN: MainActivity
}


def get_activity(activity: ActivityType, surface: Surface, game_events: GameEvents, game_stats: GameStats):
    if isinstance(activity, ActivityType):
        return ACTIVITIES[activity](surface, game_events, game_stats)
