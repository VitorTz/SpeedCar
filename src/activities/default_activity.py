from abc import ABC, abstractmethod
from ..settings import SURFACE_INFO
from ..game_events import GameEvents
from ..game_stats import GameStats
import pygame


class Activity(ABC):

    @abstractmethod
    def __init__(self,
                 surface: pygame.Surface,
                 game_events: GameEvents,
                 game_stats: GameStats,
                 label: str = "Activity"
                 ):
        self.surface = surface
        self.surface_rect = self.surface.get_rect()
        self.label = label
        self.bg_color = SURFACE_INFO["color"]
        self.__game_events = game_events
        self.__game_stats = game_stats

    @property
    def game_events(self):
        return self.__game_events

    @game_events.setter
    def game_events(self, events: GameEvents):
        if isinstance(events, GameEvents):
            self.__game_events = events

    @property
    def game_stats(self):
        return self.__game_stats

    @game_stats.setter
    def game_stats(self, stats: GameStats):
        if isinstance(stats, GameStats):
            self.__game_stats = stats

    @property
    def surface(self):
        return self.__surface

    @surface.setter
    def surface(self, surface: pygame.Surface):
        if isinstance(surface, pygame.Surface):
            self.__surface = surface

    @property
    def surface_rect(self):
        return self.__surface_rect

    @surface_rect.setter
    def surface_rect(self, rect: pygame.Rect):
        if isinstance(rect, pygame.Rect):
            self.__surface_rect = rect

    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self, label: str):
        if isinstance(label, str):
            self.__label = label.strip().title()

    @property
    def bg_color(self):
        return self.__bg_color

    @bg_color.setter
    def bg_color(self, color: tuple):
        if isinstance(color, tuple) and len(color) == 3:
            is_valid_color = True
            for i in color:
                if not (isinstance(i, int) and i in range(256)):
                    is_valid_color = False
                    break
            if is_valid_color:
                self.__bg_color = color

    def fill_surface(self):
        self.surface.fill(self.bg_color)

    def update_screen(self):
        pygame.display.update()

    @abstractmethod
    def draw_activity(self):
        pass
