import pygame


class GameEvents:

    def __init__(self):
        self.__valid_keys = {
            pygame.K_SPACE: False,
            pygame.K_x: False,
            pygame.K_c: False
        }

    @property
    def valid_keys(self):
        return self.__valid_keys.copy()

    def __set_status_valid_key(self, key: pygame.key, status: bool):
        if key in self.valid_keys.keys():
            self.__valid_keys[key] = status

    def get_status_key(self, key: pygame.key):
        return self.__valid_keys.get(key)

    def check_events(self) -> bool:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                return False
            elif e.type == pygame.KEYDOWN:
                self.__set_status_valid_key(e.key, True)
            elif e.type == pygame.KEYUP:
                self.__set_status_valid_key(e.key, False)
        return True