import pygame
from pathlib import Path


def load_rect(surface: pygame.Surface, center: int, bottom: int) -> pygame.Rect:
    if isinstance(surface, pygame.Surface):
        rect = surface.get_rect()
        rect.centerx, rect.bottom = center, bottom
        return rect


def load_image(path: Path) -> pygame.Surface:
    try:
        if not path.exists():
            raise ValueError(f"Path para o arquivo {path.absolute()} não encontrado")
        return pygame.image.load(path)
    except Exception as e:
        print(f"Não foi possível carregar a imagem {path}. Erro -> {e}")


def draw_image(main_surface: pygame.Surface, surface: pygame.Surface, rect: pygame.Rect):
    try:
        main_surface.blit(
            surface,
            rect
        )
    except Exception as e:
        print(e)


class ImageList:

    def __init__(self, images: list[Path], center: int = 0, bottom: int = 0):
        self.__rect = None
        self.__is_moving: bool = True
        self.__index: int = 0
        self.__total_imgs: int = 0
        self.__images: list[pygame.Surface] = self.__load_images(images, center, bottom)

    def __load_images(self, images: list[Path], center: int, bottom: int) -> list[pygame.Surface]:
        imgs = []
        for image in images:
            image = load_image(image)
            if image:
                if not self.__rect:
                    self.__rect = load_rect(image, center, bottom)
                imgs.append(image)
        self.__total_imgs = len(imgs)
        return imgs

    @property
    def rect(self):
        return self.__rect

    @property
    def is_moving(self):
        return self.__is_moving

    @is_moving.setter
    def is_moving(self, value: bool):
        if isinstance(value, bool):
            self.__is_moving = bool

    def __move(self):
        if self.__is_moving:
            self.__images += 1
            if self.__index >= self.__total_imgs:
                self.__index = 0

    def draw(self, surface: pygame.Surface):
        self.__move()
        draw_image(surface, self.__images[self.__index], self.__rect)



