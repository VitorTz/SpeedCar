from pathlib import Path

SURFACE_INFO = {
    "width": 800,
    "height": 700,
    "name": "SpeedCar",
    "color": (0, 0, 0),
    "icon": "res/app_icons/CSR_Racing_icon-icons.com_75325.ico"
}

FONT = {
    "monogram": Path("res/fonts/Menu/monogram.ttf"),
    "wonder": Path("res/fonts/Menu/8-BIT WONDER.TTF")
}