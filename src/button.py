from .text import get_image_text, get_font, Path
from .image import draw_image, load_rect
import pygame


class Button:

    def __init__(self, font_path: Path, center: int, bottom: int, txt: str = ""):
        self.__txt = self.__original_txt = txt
        self.__size = 40
        self.__color = (230, 230, 230)
        self.__center, self.__bottom = center, bottom
        self.__font = get_font(font_path, self.__size)
        self.__font.size(3)
        self.__image = get_image_text(self.__txt, self.__color, self.__font, size=self.__size)
        self.__rect = load_rect(self.__image, center, bottom)

    @property
    def image(self):
        return self.__image

    @image.setter
    def image(self, image: pygame.Surface):
        if isinstance(image, pygame.Surface):
            self.__image = image

    @property
    def rect(self):
        return self.__rect

    @rect.setter
    def rect(self, rect: pygame.Rect):
        if isinstance(rect, pygame.Rect):
            self.__rect = rect

    def draw(self, surface: pygame.Surface):
        draw_image(surface, self.image, self.rect)

class Buttons:

    def __init__(self, font_path: Path):
        self.__font_path
        self.__buttons: list[Button] = []

    def add(self, ):