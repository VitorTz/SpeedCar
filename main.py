import pygame
from src.load_surface import load_surface
from src.activities.get_activity import ActivityType, get_activity, GameStats, GameEvents


def main():
    clock = pygame.time.Clock()
    surface = load_surface()
    game_events = GameEvents()
    game_stats = GameStats()
    main_activity = get_activity(ActivityType.MENU, surface, game_events, game_stats)
    current_activity = main_activity

    while game_events.check_events():
        current_activity.draw_activity()
        clock.tick(60)


if __name__ == "__main__":
    pygame.init()
    pygame.font.init()
    main()